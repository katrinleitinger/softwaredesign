package at.campus02.nowa.swd.klausur1.aufgabe2;

public class PekingeseDog extends Dog {
	

	@Override
	public int getSize() {
		return 2;
	}

	@Override
	public String getDuty() {
		return "Schosshund";
	}

	@Override
	public int getLoudness() {
		return 10;
	}

}
