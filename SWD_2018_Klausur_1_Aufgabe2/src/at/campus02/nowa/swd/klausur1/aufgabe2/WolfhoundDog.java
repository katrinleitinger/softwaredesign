package at.campus02.nowa.swd.klausur1.aufgabe2;

public class WolfhoundDog extends Dog{

	@Override
	public int getSize() {
		return 10;
	}

	@Override
	public String getDuty() {
		return "Leute erschrecken";
	}

	@Override
	public int getLoudness() {
		return 5;
	}

}
