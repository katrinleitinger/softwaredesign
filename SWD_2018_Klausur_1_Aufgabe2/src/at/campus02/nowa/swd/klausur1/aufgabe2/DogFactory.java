package at.campus02.nowa.swd.klausur1.aufgabe2;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

public class DogFactory {	

	public static Dog getDog(DogBreed breed) {
		
		//L�sung Fladischer
		Map<DogBreed, Class<?>> dogs = new HashMap<>();
		dogs.put(DogBreed.PEKINGESE, PekingeseDog.class);
		dogs.put(DogBreed.FOXHOUND, FoxhoundDog.class);
		dogs.put(DogBreed.WOLFHOUND, WolfhoundDog.class);
		
		Class<?> cls = dogs.get(breed);
		try {
			Constructor<?> constr = cls.getConstructor();
			return (Dog) constr.newInstance();
		} catch (NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
		
		
		//L�sung passt, aber mit Map is eleganter
		/*
		Dog d = null;		
		switch(breed){
		case PEKINGESE:
			d = new PekingeseDog();
			break;
		case FOXHOUND:
			d = new FoxhoundDog();
			break;
		case WOLFHOUND:
			d = new WolfhoundDog();
			break;
		default:
			System.out.println("Invalid selection");
			break;
		}
		return d;
		*/
	}
		

}
