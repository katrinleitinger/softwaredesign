package at.campus02.nowa.swd.klausur1.aufgabe2;

public class FoxhoundDog extends Dog{

	@Override
	public int getSize() {
		return 6;
	}

	@Override
	public String getDuty() {
		return "Im K�rbchen schlafen";
	}

	@Override
	public int getLoudness() {
		return 3;
	}

}
