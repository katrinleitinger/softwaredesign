package at.campus02.nowa.swd.klausur1.aufgabe2;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class Tests {
	
	@Test
	public void testPekingeseDogFactory() {
		Dog c = DogFactory.getDog(DogBreed.PEKINGESE);
		assertEquals(PekingeseDog.class, c.getClass());
	}
	@Test
	public void testFoxhoundDogFactory() {
		Dog c = DogFactory.getDog(DogBreed.FOXHOUND);
		assertEquals(FoxhoundDog.class, c.getClass());
	}
	@Test
	public void testWolfhoundDogFactory() {
		Dog c = DogFactory.getDog(DogBreed.WOLFHOUND);
		assertEquals(WolfhoundDog.class, c.getClass());
	}

	@Test
	public void testPekingeseDogValues() {
		Dog c = DogFactory.getDog(DogBreed.PEKINGESE);
		assertEquals(2, c.getSize());
		assertEquals(10, c.getLoudness());
		assertEquals("Schosshund", c.getDuty());
	}

	@Test
	public void testFoxhoundDogValues() {
		Dog c = DogFactory.getDog(DogBreed.FOXHOUND);
		assertEquals(FoxhoundDog.class, c.getClass());
		assertEquals(6, c.getSize());
		assertEquals(3, c.getLoudness());
		assertEquals("Im K�rbchen schlafen", c.getDuty());
	}

	@Test
	public void testWolfhoundDogValues() {
		Dog c = DogFactory.getDog(DogBreed.WOLFHOUND);
		assertEquals(WolfhoundDog.class, c.getClass());
		assertEquals(10, c.getSize());
		assertEquals(5, c.getLoudness());
		assertEquals("Leute erschrecken", c.getDuty());
	}
}
