package at.campus02.nowa.swd.klausur1.aufgabe2;

public abstract class Dog {
	
	public abstract int getSize();

	public abstract String getDuty();

	public abstract int getLoudness();

}
