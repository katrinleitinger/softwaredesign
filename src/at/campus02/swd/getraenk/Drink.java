package at.campus02.swd.getraenk;

public abstract class Drink {
	
	private String name;	

	public void setName(String name) {
		this.name = name;
	}

	public void make() {		
		boilWater();
		fillCup();
		addIngredients();
		serveDrink();
	}
	
	//wenn man Schritte �ndern will, am besten hier auf protected setzen
	//Basisimplementierung kann bleiben und wird auch gemacht, aber kann in
	//abgeleiteten Klassen ge�ndert werden
	private void boilWater() {
		System.out.println("Wasser kochen");
	}
	
	private void fillCup() {
		System.out.println("Wasser in Tasse einf�llen");
	}
	
	protected abstract void addIngredients();
	
	private void serveDrink() {		
		System.out.println(name +" servieren");
	}
	

}
