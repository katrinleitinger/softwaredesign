package at.campus02.swd.getraenk;

public class Coffee extends Drink {
	

	public Coffee() {
		setName("Kaffee");
	}

	@Override
	protected void addIngredients() {
		System.out.println("Pulverkaffee in die Tasse");
		
	}

}
