package at.campus02.swd.getraenk;

public class Tea extends Drink {
	
	public Tea() {
		setName("Tee");
	}

	@Override
	protected void addIngredients() {
		System.out.println("Teebeutel in die Tasse");
		
	}

}
