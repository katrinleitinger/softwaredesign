package at.campus02.swd.getraenk;

public class Main {

	public static void main(String[] args) {


		/*
		Drink coffee = new Coffee();
		Drink tea = new Tea();
		
		coffee.make();
		System.out.println();
		tea.make();
		*/
		
		//um zu testen, ob es wirklich m�glich ist, ein Getr�nk zuzubereiten, ohne zu wissen, was es genau ist
		
		test(new Coffee());
		System.out.println();
		test(new Tea());
				
	}

	private static void test(Drink d) {
		d.make();
		
	}

}
