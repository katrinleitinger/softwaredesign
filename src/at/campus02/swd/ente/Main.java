package at.campus02.swd.ente;

public class Main {

	public static void main(String[] args) {

		Ente maxi = new Ente("Maxi", new Langsam());
		Ente susi = new Ente("Susi", new Normal());
		Ente frida = new Ente("Frida", new Schnell());
		
		
		maxi.gehen();
		susi.quaken();
		frida.quaken();
		
		maxi.fliegen();
		susi.fliegen();
		frida.fliegen();
		

	}

}
