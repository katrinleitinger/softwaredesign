package at.campus02.swd.ente;

public class Langsam implements Flugart {

	@Override
	public void fliegen(String name) {
		System.out.println(name + " fliegt langsam");
		
	}
}
