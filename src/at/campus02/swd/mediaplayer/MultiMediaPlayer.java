package at.campus02.swd.mediaplayer;

import java.util.HashMap;

public class MultiMediaPlayer {
	
	private HashMap<String, IPlayer> hm = new HashMap<>();

	
	public void registerFormat(String string, IPlayer player) {
			hm.put(string, player);			
	}

	public void play(String string, String string2) {
		if(hm.containsKey(string)) {			
		hm.get(string).play(string2);
		}
		else {
			System.out.println("file format "+string+" not recognized");
		}
		
	}


}
