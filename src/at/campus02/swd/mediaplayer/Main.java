package at.campus02.swd.mediaplayer;

public class Main {

	public static void main(String[] args) {
		
		
		MultiMediaPlayer player = new MultiMediaPlayer();
		player.registerFormat("mp4", new Mp4Player());
		player.registerFormat("mpeg", new MpegPlayer());
		player.registerFormat("mp3", new Mp3Player());
		
		
		player.play("mp4", "C://temp/test.mp4");
		player.play("mpeg", "C://temp/test.mpeg");
		player.play("mkv", "C://temp/test.mkv");
		player.play("mp3", "C://temp.test.mp3");
		
	}

}
