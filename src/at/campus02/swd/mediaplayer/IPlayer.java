package at.campus02.swd.mediaplayer;

public interface IPlayer {
	
	void play(String file);
}
