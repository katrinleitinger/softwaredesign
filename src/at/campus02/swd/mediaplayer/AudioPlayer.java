package at.campus02.swd.mediaplayer;

public class AudioPlayer {

	public void playMp3(String file) {
		System.out.println("Now playing mp3: " + file);
	}
	
	public void playMp2(String file) {
		System.out.println("Now playing mp2: " + file);
	}
}