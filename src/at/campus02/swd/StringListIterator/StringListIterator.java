package at.campus02.swd.StringListIterator;

import java.util.Iterator;

public class StringListIterator implements Iterator<String> {

	private String[] words;
	//richtige Reihenfolge
	//private int position = 0;
	
	//verkehrte Reihenfolge
	private int position;
		
	
	public StringListIterator(String[] words) {
		super();
		this.words = words;
		//verkehrt herum, position im Constructor definieren
		this.position = words.length -1;
	}

	@Override
	public boolean hasNext() {
		//richtig herum
		//return words.length > position;
		
		//verkehrt herum
		return position >= 0;
	}

	@Override
	public String next() {
		//richtig herum
		//return words[position++];
		
		//verkehrt herum
		return words[position--];
	}

}
