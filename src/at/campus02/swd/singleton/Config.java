package at.campus02.swd.singleton;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class Config {
	
	//das macht singleton aus
	private static Config myConfig;
	
	public static Config instance() {
		
		//check, damit Config nur einmal instanziert wird
		if(myConfig == null) {
			myConfig = new Config();
		}
		return myConfig;
	}
	
	
	private String username;
	private String connectionString;

	//constructor private setzen
	private Config() {
		
		
		try {
			FileInputStream stream = new FileInputStream("config.properties");
			
			Properties p = new Properties();
			p.load(stream);
			
			//quasi ein log, um zu sehen, wie oft das config file gelesen wird (sprich wie oft wird die Klasse instanziert)
			System.out.println("Reading config.properties...");
			
			//properties file ist im Prinzip ein Set (key und value)
			//daten auslesen und membervariablen zuweisen
			this.connectionString = p.getProperty("connectionstring", "<defaultconnection>");
			this.username = p.getProperty("username", "<default>");
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	
	public String getConnectionString() {
		
		return connectionString;
	}
	
	
	public String getUserName() {
		
		return username;
	}
}
