package at.campus02.swd.singleton;

public class Main {
	
	//mit anlegen der static variable wird config file nur mehr einmal instanziert, aber nur in der Main Klasse
	//is bl�d, wenn man mehrere Klassen hat
	//private static Config myConfig;

	public static void main(String[] args) {
		
		//myConfig = new Config();
		
		//Config.myConfig = null;
		
		printUserName();
		printConnection();

	}

	private static void printConnection() {
		
		//Config config = new Config();
		//System.out.println("Username: " + myConfig.getConnectionString());
		
		System.out.println("Connection: " + Config.instance().getConnectionString());
		
	}

	private static void printUserName() {
		
		//Config config = new Config();		
		//String username = myConfig.getUserName();
		//System.out.println("Username: " + username);
		
		System.out.println("Username: " + Config.instance().getUserName());
		
	}

}
