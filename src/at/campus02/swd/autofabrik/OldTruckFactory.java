package at.campus02.swd.autofabrik;

public class OldTruckFactory {

	public Truck orderTruck() {
		
		return new Truck();
	}
	
	public void deliver(Truck truck) {
		System.out.println("deliver truck");
	}
	
	public void cleanTruck(Truck truck) {
		System.out.println("clean truck");
	}
}
