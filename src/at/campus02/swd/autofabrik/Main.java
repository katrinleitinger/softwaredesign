package at.campus02.swd.autofabrik;

public class Main {

	public static void main(String[] args) {
		
		Fabrik dl = new FabrikDeutschland();
		dl.bestellen("BMW");
		
		Fabrik en = new FabrikEngland();
		en.bestellen("MonsterTruck");
		
		//bisher
		/*
		OldTruckFactory oldFactory = new OldTruckFactory();
		Truck truck = oldFactory.orderTruck();
		oldFactory.cleanTruck(truck);
		oldFactory.deliver(truck);
		*/
		
		//gewünschtes Verhalten (Truck kann verändert werden, OldTruckFactory nicht!)
		dl.bestellen("OldTruck");
		en.bestellen("OldTruck");

	}

}
