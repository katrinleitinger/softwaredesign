package at.campus02.swd.autofabrik;

public class FabrikDeutschland extends Fabrik {

	@Override
	protected Auto erzeugeAuto(String fahrzeug) {
		System.out.println("Wir bauen einen BMW");
		return new BMW();
	}

	@Override
	protected void autoReinigen(Auto auto) {
		super.autoReinigen(auto);
		System.out.println("aber gaaanz gr�ndlich");
	}
	
	


}
