package at.campus02.swd.autofabrik;

public abstract class Fabrik {

	public Auto bestellen(String fahrzeug) {
		
		if(fahrzeug.equals("OldTruck")) {
			OldTruckFactory otf = new OldTruckFactory();
			Truck truck = otf.orderTruck();
			otf.cleanTruck(truck);
			otf.deliver(truck);
			return truck;
		}
		
		
		//Objekt erzeugen => Factory Method
		Auto auto = erzeugeAuto(fahrzeug);
		
		
		//Schritte zum Zusammenbauen ausführen =>
		autoReinigen(auto);
		autoAusliefern(auto);
		
		return auto;
		
	}

	private void autoAusliefern(Auto auto) {
		System.out.println("Auto ausliefern");
		
	}

	//wieder protected, damit man methoden in subklassen überschreiben kann
	protected void autoReinigen(Auto auto) {
		System.out.println("Auto reinigen");
		
	}

	protected abstract Auto erzeugeAuto(String fahrzeug);
		
	

}
