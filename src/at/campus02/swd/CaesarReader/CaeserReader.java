package at.campus02.swd.CaesarReader;

import java.io.FilterReader;
import java.io.IOException;
import java.io.Reader;

public class CaeserReader extends FilterReader {
	
	protected char key;

	protected CaeserReader(Reader arg0) {
		super(arg0);
	}
	
	public void setKey(char key) {
		this.key = key;
	}

	//zum decodieren, key abziehen
	@Override
	public int read() throws IOException {
		int c = super.read();
		if(c == -1) {
			return c;
		}
		return c - (int) key;
	}

	



	


}
