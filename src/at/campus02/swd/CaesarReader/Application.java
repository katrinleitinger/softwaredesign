package at.campus02.swd.CaesarReader;

import java.io.FileReader;
import java.io.IOException;

public class Application {

	public static void main(String[] args) throws IOException {
		
		CaeserReader cr = new CaeserReader(new FileReader("geheim.txt"));
		int x;
		cr.setKey((char) 14);
		while((x = cr.read()) != -1) {
			System.out.print((char) x);		
		}
		
	}

}
