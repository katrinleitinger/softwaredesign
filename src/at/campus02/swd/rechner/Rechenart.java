package at.campus02.swd.rechner;

public interface Rechenart {
	
	public int berechne(int wert1, int wert2);
}
