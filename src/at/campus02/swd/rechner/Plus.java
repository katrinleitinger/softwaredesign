package at.campus02.swd.rechner;

public class Plus implements Rechenart {

	@Override
	public int berechne(int wert1, int wert2) {
		return wert1 + wert2;
	}

}
