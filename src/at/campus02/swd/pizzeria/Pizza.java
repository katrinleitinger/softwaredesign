package at.campus02.swd.pizzeria;

public interface Pizza {
	
	public default void zubereiten() {
		teig();
		belegen();
		backen();
	}
	
	
	public default void teig() {
		System.out.println("Wir kneten den Teig");
	}
	
	public abstract void belegen();
	
	public default void backen() {
		System.out.println("Wir schieben die Pizza in den Ofen");
	}
	
	
	
}
