package at.campus02.swd.pizzeria;

public class Hawaii implements Pizza {

	public void belegen() {
		System.out.println("Tomatensauce, K�se, Schinken und Ananas drauf");
	}

	@Override
	public void backen() {
		System.out.println("Und weil wir ganz speziell sind, geht das backen bei der Hawaii anders...");
	}
	
	

}
