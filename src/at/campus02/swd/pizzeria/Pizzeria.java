package at.campus02.swd.pizzeria;

public class Pizzeria {

	public Pizza bestelle(String p) {
		Pizza pizza = null;
		
		while(pizza == null) {		
		switch(p) {
		case "Margherita":
			pizza = new Margherita();
			break;
		case "Funghi":
			pizza = new Funghi();
			break;
		case "Hawaii":
			pizza = new Hawaii();
			break;
		}
		}
		
		pizza.zubereiten();
		
		return pizza;
	}

}
