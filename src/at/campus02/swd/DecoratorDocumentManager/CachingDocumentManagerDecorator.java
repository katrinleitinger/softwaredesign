package at.campus02.swd.DecoratorDocumentManager;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CachingDocumentManagerDecorator extends AbstractDocumentManagerDecorator {
	
	Map<String, String> cache = new HashMap<String, String>();

	public CachingDocumentManagerDecorator(IDocumentManager dm) {
		super(dm);
	}

	@Override
	public String load(String fileName) throws IOException {
		//schauen, ob die Datei im Cache ist
		if(cache.containsKey(fileName)) {
			System.out.println("Aus Cache: "+fileName);
			return cache.get(fileName);
		}
		//falls die Datei nicht im Cache ist, laden und im Cache ablegen
		System.out.println("Von HDD: " + fileName);
		String content = super.load(fileName);
		cache.put(fileName, content);
		return content;
	}

	@Override
	public void saveOrUpdate(String fileName, String document) throws IOException {
		super.saveOrUpdate(fileName, document);
		//wenn die Datei ge�ndert wurde, fliegt sie aus dem Cache
		//cache.remove(fileName);
		
		//alternativ Datei im Cache updaten
		cache.put(fileName, document);
	}
	
	//Decorator kann auch zus�tzliche Methoden zur Verf�gung stellen
	public void flushCache() {
		cache.clear();
	}
	
	

}
