package at.campus02.swd.DecoratorDocumentManager;

import java.io.IOException;

public abstract class AbstractDocumentManagerDecorator implements IDocumentManager {

	protected IDocumentManager dm;
	
	public AbstractDocumentManagerDecorator(IDocumentManager dm) {
		super();
		this.dm = dm;
	}

	@Override
	public String load(String fileName) throws IOException {
		return dm.load(fileName);
	}

	@Override
	public void saveOrUpdate(String fileName, String document) throws IOException {
		dm.saveOrUpdate(fileName, document);
	}

}
