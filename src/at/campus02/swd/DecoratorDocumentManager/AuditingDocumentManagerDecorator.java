package at.campus02.swd.DecoratorDocumentManager;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class AuditingDocumentManagerDecorator extends AbstractDocumentManagerDecorator {

	//L�sung Fladischer mit log Datei
	private Writer logger;
	
	public AuditingDocumentManagerDecorator(IDocumentManager dm, Writer logger) throws IOException {
		super(dm);
		this.logger = logger;
	}

	@Override
	public String load(String fileName) throws IOException {
		logger.write("Datei geladen: " + fileName + "\n");
		String content =  super.load(fileName);
		logger.write("Datei fertig geladen: " + fileName + "\n");
		return content;
	}

	@Override
	public void saveOrUpdate(String fileName, String document) throws IOException {
		logger.write("Datei wird geschrieben: " + fileName + "\n");
		super.saveOrUpdate(fileName, document);
		logger.write("Datei fertig geschrieben: " + fileName + "\n");
	}
	
	
	
	//meine L�sung mit sysout
	/*
	public AuditingDocumentManagerDecorator(IDocumentManager dm) {
		super(dm);
	}

	@Override
	public String load(String fileName) throws IOException {
		System.out.println("Datei "+ fileName + " wurde geladen");
		return super.load(fileName);
	}

	@Override
	public void saveOrUpdate(String fileName, String document) throws IOException {
		System.out.println("Datei "+ fileName + " wurde ge�ndert");
		System.out.println("Neuer Inhalt: " + document);
		super.saveOrUpdate(fileName, document);
	}
	*/
	

}
