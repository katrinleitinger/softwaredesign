package at.campus02.swd.DecoratorDocumentManager;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Date;

public class Application {

	public static void main(String[] args) {
		
		try(Writer logger = new FileWriter("logfile.txt")){
		
		DocumentManager dmo = new DocumentManager();
		IDocumentManager cdmd = new CachingDocumentManagerDecorator(dmo);
		AuditingDocumentManagerDecorator dm = new AuditingDocumentManagerDecorator(cdmd, logger);
		
		//meine L�sung
		//IDocumentManager dm2 = new AuditingDocumentManagerDecorator(dmo);
		try {
			System.out.println(dm.load("test.txt"));
			System.out.println(dm.load("test.txt"));
			System.out.println(dm.load("test.txt"));
			System.out.println(dm.load("test.txt"));
			System.out.println(dm.load("test.txt"));
			System.out.println(dm.load("test.txt"));
			//dm.load("test.txt");
			dm.saveOrUpdate("uhrzeit.txt", new Date().toString());
			System.out.println(dm.load("uhrzeit.txt"));
			System.out.println(dm.load("uhrzeit.txt"));
			System.out.println(dm.load("uhrzeit.txt"));
			
			//meine L�sung
			/*
			dm2.load("test.txt");
			dm2.saveOrUpdate("uhrzeit.txt", "keine uhrzeit mehr drin");
			*/
			
		} catch (IOException e) {
			System.out.println("Konnte keine Daten verarbeiten!");
			e.printStackTrace();
		}

	} catch (IOException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}

}
}
