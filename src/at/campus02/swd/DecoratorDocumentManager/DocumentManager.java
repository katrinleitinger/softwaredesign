package at.campus02.swd.DecoratorDocumentManager;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;


public class DocumentManager implements IDocumentManager {

	@Override
	public String load(String fileName) throws IOException {
		//geht mit Java 10 einfacher mit InputStream und readAllBytes-Methode
		BufferedReader inp = new BufferedReader(new FileReader(fileName)) ;
		String content =  "";
		String line;
		while((line = inp.readLine()) != null) {
			content = content + line;
		}
		inp.close();
		return content;
	}

	@Override
	public void saveOrUpdate(String fileName, String document) throws IOException {
		Writer outp = new FileWriter(fileName);
		outp.write(document);
		outp.close();
	}

}
