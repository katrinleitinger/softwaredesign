package at.campus02.swd.DecoratorDocumentManager;

import java.io.IOException;

public interface IDocumentManager {
	
	public String load(String fileName) throws IOException;
	public void saveOrUpdate(String fileName, String document) throws IOException;

}
