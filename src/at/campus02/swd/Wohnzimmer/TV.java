package at.campus02.swd.Wohnzimmer;

public class TV {

	private boolean isOn = false;
	private int channel = 1;
	
	
	
	public int getChannel() {
		return channel;
	}

	public void setChannel(int channel) {
		this.channel = channel;
	}

	public void on() {
		isOn = true;
		System.out.println("TV ein: " + channel);
	}
	
	public void off() {
		isOn = false;
		System.out.println("TV aus");
	}
	
	public void up() {
		if(!isOn) {
			on();
		}
		if(channel == 40) {
			channel = 1;
		}
		else {
			channel++;
		}		
		System.out.println("Neuer Kanal: " + channel);
	}
	
	public void down() {
		if(!isOn) {
			on();
		}
		if(channel == 1) {
			channel = 40;
		}
		else {
			channel--;
		}		
		System.out.println("Neuer Kanal: " + channel);
	}
	
	public boolean isOn() {
		return isOn;
	}
}
