package at.campus02.swd.Wohnzimmer;

public class LightToggleCommand implements ICommand {
	
	private Light light;

	public LightToggleCommand(Light light) {
		super();
		this.light = light;
	}


	@Override
	public void execute() {
		if(light.isOn()) {
			light.off();
		}
		else {
			light.on();
		}

	}

}
