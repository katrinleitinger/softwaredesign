package at.campus02.swd.Wohnzimmer;

public class Light {
	
	private int dimFactor = 0;
	
	public void on() {
		dimFactor = 100;
		System.out.println("Leuchtet mit "+ dimFactor + "%");
	}
	
	public void off() {
		dimFactor = 0;
		System.out.println("Leuchtet mit "+ dimFactor + "%");
	}
	
	public void dim(int dimFactor) {
		if(dimFactor < 0 || dimFactor > 100) {
			System.out.println("Kein g�ltiger Dim-Faktor: " + dimFactor + "%");
			return;
		}
		this.dimFactor = dimFactor;
		System.out.println("Leuchtet mit "+ dimFactor + "%");
	}
	
	public boolean isOn() {
		return dimFactor > 0;
	}

}
