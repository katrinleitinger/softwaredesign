package at.campus02.swd.Wohnzimmer;

public class TVCommand implements ICommand {
	
	TV tv = new TV();

	public TVCommand(TV tv) {
		super();
		this.tv = tv;
	}

	@Override
	public void execute() {
		if(!tv.isOn()) {
			if(tv.getChannel() == 40) {
				tv.setChannel(1);
			}
			else {
				tv.up();
			}
		}
	}	

}
