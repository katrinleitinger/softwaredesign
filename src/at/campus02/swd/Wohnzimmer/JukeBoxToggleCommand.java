package at.campus02.swd.Wohnzimmer;

public class JukeBoxToggleCommand implements ICommand {
	
	private JukeBox jb;

	public JukeBoxToggleCommand(JukeBox jb) {
		super();
		this.jb = jb;
	}


	@Override
	public void execute() {
		if(jb.isOn()) {
			jb.off();
		}
		else {
			jb.on();
		}

	}

}
