package at.campus02.nowa.swd.klausur1.aufgabe1;


public class Parcel {

	private Transporter transporter;
	private double weight;
	private int distance;
	
	
	public Parcel(Transporter transporter, double weight, int distance) {
		this.transporter = transporter;
		this.weight = weight;
		this.distance = distance;
	}


	public Transporter getTransporter() {
		return transporter;
	}


	public double getWeight() {
		return weight;
	}


	public int getDistance() {
		return distance;
	}


	public double calcPrice(IStrategy strategy) {
		//�bergibt Parcel (mit this)
		return strategy.calc(this);
	}
	
	

}
