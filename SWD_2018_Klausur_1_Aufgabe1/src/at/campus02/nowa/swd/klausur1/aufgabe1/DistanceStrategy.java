package at.campus02.nowa.swd.klausur1.aufgabe1;

public class DistanceStrategy implements IStrategy {

	@Override
	public double calc(Parcel p) {
		return p.getDistance()*p.getTransporter().getKilometerFee();
	}

}
