package at.campus02.nowa.swd.klausur1.aufgabe1;

public interface IStrategy {

	public double calc(Parcel p);
}
