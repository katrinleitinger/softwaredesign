package at.campus02.nowa.swd.klausur1.aufgabe1;

public class WeightStrategy implements IStrategy {

	@Override
	public double calc(Parcel p) {
		return p.getWeight()*p.getTransporter().getKilogramFee();
	}

}
