package at.campus02.nowa.swd.klausur1.aufgabe1;

public class Transporter {

	private String name;
	private double kilogramFee;
	private double kilometerFee;
	
	
	
	public Transporter(String name, double kilometerFee, double kilogramFee) {
		this.name = name;
		this.kilogramFee = kilogramFee;
		this.kilometerFee = kilometerFee;
	}
	public String getName() {
		return name;
	}
	public double getKilogramFee() {
		return kilogramFee;
	}
	public double getKilometerFee() {
		return kilometerFee;
	}
	
	
}
